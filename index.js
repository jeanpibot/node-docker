const express = require('express');
const path = require('path');
const cors = require('cors');
const { connection, close } = require('./services/rethinkdb/rethinkdb');
const productsRouter = require('./Routes/views/products');
const productsApiRouter = require('./Routes/api/products');
// Initial app
const app = express();
// initial port
const port = process.env.PORT || 3000;

// Cors
app.use(cors());
// Middlewares globales
app.use(express.json());

// static files
app.use('/static', express.static(path.join(__dirname, 'public')));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// open the connection
app.use(connection);

// Routes
app.use('/products', productsRouter);
app.use('/api/products', productsApiRouter);

// Redirect
app.get('/', (req, res) => {
  res.redirect('/products');
});

// close the connection
app.use(close);

app.listen(port, () => console.log(`Listening on port ${port}`));
