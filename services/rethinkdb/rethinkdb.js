const r = require('rethinkdb');

const dbConfig = require('../../config/database');

const desc = r.desc('id');
const rethinkdb = r.db('test');

const connection = async (req, res, next) => {
  await r.connect(dbConfig).then((conn) => {
    req.rdb = conn;
    next();
  }).error(() => {
    return (error) => {
      res.send(500, { error: error.message });
    };
  });
};

/*
* Close the RethinkDB connection
*/
const close = async (req, res, next) => {
  await req.rdb.close();
};

module.exports = {
  rethinkdb,
  desc,
  connection,
  close,
};
