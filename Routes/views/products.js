const express = require('express');

const router = express.Router();

const { rethinkdb, desc } = require('../../services/rethinkdb/rethinkdb');

router.get('/', async (req, res, next) => {
  try {
    const products = await rethinkdb.table('products').orderBy(desc).run(req.rdb);
    res.render('products', { products });
  } catch (error) {
    next(error);
  }

});

module.exports = router;
